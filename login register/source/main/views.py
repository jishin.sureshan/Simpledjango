from django.views.generic import TemplateView


class IndexPageView(TemplateView):
    template_name = 'main/index.html'


class ChangeLanguageView(TemplateView):
    template_name = 'main/change_language.html'


# @user_passes_test(lambda u: u.is_staff)
def delete_page_from_s3(request):
    # url = website_utils.clean_site_name(request.GET.get('url'))
    try:
        url = "jishin.growthplug.com"
        domain = url.split("/")[0]
        file_name = '{}.html'.format(url.split("/")[2])
        name = html.escape(file_name)
        folder = url.split("/")[1]
        # website = Website.objects.get(site__domain=domain)
        website_id = 123
        file_path = '{}/{}/{}'.format(website_id, folder, file_name)
        path = html.escape(file_path)
        if folder and file_name and website:
            # page = WebsitePageS3Storage()
            if file_path:
                # page.delete(file_path)
                return HttpResponse("deleted page {} from S3".format(name))
            return HttpResponse("file does not exist path:{}".format(path))
    except Exception as e:
        return HttpResponse("failed deleting page from S3 {}".format(e))
